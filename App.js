import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import SignIn from './src/signin/SignIn';
import UserDetails from './src/userdetails/UserDetails'
import Options from './src/options/Options';
import Index from './src/mainscreens/index';
import EnterPhone from './src/enterphone/EnterPhone';
import VerifyPhone from './src/enterphone/VerifyPhone';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import GoBack from './src/GoBack';
import GoogleSignIn from './src/signin/GoogleSignIn';
import WebViews from './src/signin/WebViews';
// import firebaseConfig from './src/signin/config';

var { height, width } = Dimensions.get('window');


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
    const Stack = createStackNavigator();
    return (
      <View style={styles.container}>
      {/*  <GoogleSignIn />
        {/* <WebViews />
         <NavigationContainer>
          <Stack.Navigator headerMode="none">
            <Stack.Screen name="Sign In" component={SignIn} />
            <Stack.Screen name="Enter Phone" component={EnterPhone} />
            <Stack.Screen name="Verify Phone" component={VerifyPhone} />
            <Stack.Screen name="User Details" component={UserDetails} />
            <Stack.Screen name="Options" component={Options} />
          </Stack.Navigator>
        </NavigationContainer>
        <SignIn />
        <UserDetails />
        <Options />
        <EnterPhone />
        <VerifyPhone />
        <SignIn />
        <SignIn />
        <VerifyPhone />*/}

        <NavigationContainer>
         <Stack.Navigator headerMode="none">
           <Stack.Screen name="Sign In" component={SignIn} />
           <Stack.Screen name="Enter Phone" component={EnterPhone} />
           <Stack.Screen name="Verify Phone" component={VerifyPhone} />
           <Stack.Screen name="User Details" component={UserDetails} />
           <Stack.Screen name="Options" component={Options} />
           <Stack.Screen name="Index" component={Index} />
         </Stack.Navigator>
       </NavigationContainer>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: height,
    alignItems: 'stretch',
    justifyContent: 'center',
  }
})
