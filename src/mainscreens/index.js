// Mapbox access token - sk.eyJ1IjoiYWc2MDIiLCJhIjoiY2thdHUwYjR1MzRwNTMxcDZxeW15b3JwOSJ9.qyf3UzDXewVj-FIRiaaNoA
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,Image,
  View,
  TouchableOpacity,
} from 'react-native';
import { NavigationBar } from 'navigationbar-react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import MapHome from './maphome'


{/*------------------------------HEADER------------------------------------- */}
const ComponentLeft = () => {
  return(
    <View style={{ flex: 1, alignItems: 'flex-start'}} >
       <TouchableOpacity style={ {justifyContent:'center', flexDirection: 'row'}}>
        <Icon style = {{marginLeft:15 }} name="user" size={30} color='white'/>
        <Text style={{ color: 'white', }}></Text>
      </TouchableOpacity>
    </View>
  );
};

const ComponentCenter = () => {
  return(
    <View style={{ flex: 1, }}>
       <Text style = {{ color: 'white', alignSelf:'center', fontSize:20 }}>Cab</Text>
    </View>
  );
};

const ComponentRight = () => {
  return(
    <View style={{ flex: 1, alignItems: 'flex-end', }}>
      <TouchableOpacity>
      <Icon style = {{marginRight:15 }} name="bell" size={30} color='white'/>
      </TouchableOpacity>
    </View>
  );
};
{/*------------------------------FOOTER-------------------------------------*/}
function Home() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <MapHome />
    </View>
  );
}

function Buzz() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Buzz!</Text>
    </View>
  );
}

function MyTrukos() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>My Trukos!</Text>
    </View>
  );
}

function SafetyGuard() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Safety Guard!</Text>
    </View>
  );
}


const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#8c4d79',
      }}
    >
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: "Let's Trukofy",
          tabBarIcon: () => (
            <Icon name="home" size={25} color='#8c4d79'/>
          ),
        }}
      />
      <Tab.Screen
        name="Buzz"
        component={Buzz}
        options={{
          tabBarLabel: 'Buzz',
          tabBarIcon: ({ tintColor }) => (
            <Icon name="comments" size={25} color='#8c4d79'/>
          ),
        }}
      />
      <Tab.Screen
        name="MyTrukos"
        component={MyTrukos}
        options={{
          tabBarLabel: 'My trukos',
          tabBarIcon: ({ tintColor }) => (
            <Icon name="users" size={25} color='#8c4d79'/>
          ),
        }}
      />
      <Tab.Screen
        name="SafetyGuard"
        component={SafetyGuard}
        options={{
          tabBarLabel: 'Safety Guard',
          tabBarIcon: ({ tintColor }) => (
            <Icon name="shield" size={25} color='#8c4d79'/>
          ),
        }}
      />
    </Tab.Navigator>
  );
}
{/*------------------------------MAIN-------------------------------------*/}

export default class Index extends Component {
  render() {
    return (
      <View style={styles.container}>
        <NavigationBar
          componentLeft     = { () =>  <ComponentLeft />   }
          componentCenter   = { () =>  <ComponentCenter /> }
          componentRight    = { () =>  <ComponentRight />  }
          navigationBarStyle= {{ backgroundColor: '#8c4d79',height:70, }}
          statusBarStyle    = {{ barStyle: 'light-content', backgroundColor: '#215e79' }}
        />
          <View style={styles.footer}>
          <NavigationContainer  independent= {true}>
          <MyTabs />
        </NavigationContainer>
          </View>

      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: 'white',
    },
    footer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'stretch',
        backgroundColor: 'white',
    },
});
