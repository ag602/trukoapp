// import * as React from 'react';
// Mapbox access token - sk.eyJ1IjoiYWc2MDIiLCJhIjoiY2thdHUwYjR1MzRwNTMxcDZxeW15b3JwOSJ9.qyf3UzDXewVj-FIRiaaNoA

import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import MapboxGL from "@react-native-mapbox-gl/maps";
import { Input } from 'react-native-elements';
import axios from 'axios';
MapboxGL.setAccessToken("sk.eyJ1IjoiYWc2MDIiLCJhIjoiY2thdHUwYjR1MzRwNTMxcDZxeW15b3JwOSJ9.qyf3UzDXewVj-FIRiaaNoA");
MapboxGL.setConnected(true);
//
function GeocoderService(){
  return('hi');
  const api = 'https://api.mapbox.com/geocoding/v5/mapbox.places';
  console.log(this.state.location);
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    marginLeft:0,
  },
  search:{
    flex:1,
    marginTop:20,
    width:350,
    alignSelf:'center'
  },
  map: {
    flex: 3,
    width:410,
    marginLeft:0,
    marginBottom:50,
    marginTop:40
  },
  button:{
    flex:1,
    width:300,
    alignSelf:'center',
    color:'#8c4d79'
  }
});

export default class MapHome extends Component {
  constructor(props) {
      super(props);
      this.state = {
          location: '',
          nameList: '',
  }
  console.log(this.state.location);
}

// make the GET request to fetch data from the URL then using promise function to handle response.
  componentDidMount() {
    MapboxGL.setTelemetryEnabled(false);
  }

  fetchData = async () =>{
    const token = 'sk.eyJ1IjoiYWc2MDIiLCJhIjoiY2thdHUwYjR1MzRwNTMxcDZxeW15b3JwOSJ9.qyf3UzDXewVj-FIRiaaNoA'
    var location = this.state.location
    var url = await axios.get(`https://api.mapbox.com/geocoding/v5/mapbox.places/${location}.json?access_token=${token}`)
    .then(res => {
      const nameList = res.data;
      this.setState({ nameList });
      // console.log(nameList['features']);
    })
  }

  render() {
    const result = this.fetchData()
    return (
      <View style={styles.container}>
        <View style={styles.search}>
          <Input onChangeText={(location) =>
              this.setState({ location })}
              placeholder='Enter Starting Location'
              leftIcon={{ type: 'font-awesome', name: 'circle-o', color:'grey' }}
            />
          <Input
          placeholder='Enter Destination'
          leftIcon={{ type: 'font-awesome', name: 'map-marker', color:'grey'  }}
          />
        </View>
          <MapboxGL.MapView zoom={0} style={styles.map} />
          <View style={styles.button}>
        <Button color = "#8c4d79"
          title="Start"
        />
        </View>
      </View>
    );
  }
}
