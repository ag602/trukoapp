//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { fonts } from '../../constants/FontSize';
import Option from './Option';
import GoBack from '../GoBack';
// create a component
class Options extends Component {
    constructor(props) {
        super(props);
        this.state = {
            array: [
                ['Bollywood', 'Hollywood', true, 1],
                ['Food', 'Drinks', true, 0],
                ['Cheesecake', 'Jalebi', true, 0],
                ['Salsa', 'Bhangra', true, 1],
                ['A', 'B', false, 0],
                ['C', 'D', false, 0],
                ['E', 'F', false, 0],
            ],
            buttonColor: ['#591e44', 'white'],
            buttonTextColor: ['white', 'black'],
        }
        this.changeOption = this.changeOption.bind(this);
        this.toggleButton = this.toggleButton.bind(this);
    }
    toggleButton = (id, ind) => {
        let array = this.state.array;
        array[id][3] = ind;
        this.setState({
            array,
        })
    }
    changeOption(id) {
        let array = this.state.array;
        let val = array[id];
        array[id] = array[4];
        array[id][2] = true;
        array.splice(4, 1);
        array.push(val);
        this.setState({
            array
        })
    }
    render() {
        let Row = [];
        let lbc, lbtc, rbc, rbtc;
        let item, keys;
        let cnt = 0;
        for (let i = 0; i < this.state.array.length; i++) {
            if (cnt == 4) {
                break;
            }
            item = this.state.array[i];
            keys = i;
            cnt++;
            let val = item[3];
            lbc = this.state.buttonColor[val];
            lbtc = this.state.buttonTextColor[val];
            rbc = this.state.buttonColor[1 - val];
            rbtc = this.state.buttonTextColor[1 - val];
            Row.push(
                <Option
                    leftVal={item[0]}
                    rightVal={item[1]}
                    key={keys}
                    keys={keys}
                    leftButtonColor={lbc}
                    leftButtonTextColor={lbtc}
                    rightButtonColor={rbc}
                    rightButtonTextColor={rbtc}
                    toggleButton={this.toggleButton}
                    changeOption={this.changeOption}
                />
            )
        }
        return (
            <View style={styles.container}>
                <View style={styles.goBackView}>
                    <GoBack
                        goToScreen='User Details'
                        navigation={this.props.navigation}
                    />
                </View>
                <View style={styles.msgView}>
                    <Text style={styles.msgText}>
                        {`Last bit about you`}
                    </Text>
                    <Text style={styles.msgText}>
                        {`Click “OR” for more options`}
                    </Text>
                </View>
                <View style={styles.optionsRowView}>
                    {Row}
                </View>
                <View style={styles.buttonView}>
                    <TouchableOpacity activeOpacity={0.5}
                        onPress={() => this.props.navigation.navigate('Index')}
                        style={styles.button}>
                        <Text style={styles.buttonText}>Lets trukofy</Text>
                    </TouchableOpacity>
                    {/* <View>
                        <View style={{
                            width: 80,
                            height: 65,
                            backgroundColor: 'black',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>

                            <View style={{
                                backgroundColor: 'white',
                                marginTop: 25,
                                marginEnd: 25,
                                height: 45,
                                width: 60,
                                alignSelf: 'center',
                                borderTopRightRadius: 40
                            }} />

                        </View>
                    </View> */}
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: 'white',
    },
    goBackView: {
        flex: 1,
    },
    msgView: {
        flex: 4,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    optionsRowView: {
        flex: 6,
        marginTop: 20,
    },
    buttonView: {
        flex: 4,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    msgText: {
        fontSize: fonts.medium,
    },
    button: {
        zIndex: 5,
        height: 50,
        width: 200,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#000000',
        borderWidth: 1,
    },
    buttonText: {
        color: '#000000',
        fontSize: fonts.medium,
        fontWeight: 'bold',
    },
});

//make this component available to the app
export default Options;
