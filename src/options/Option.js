//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { fonts } from '../../constants/FontSize';

// create a component
class Option extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.leftButtonView}>
                    <TouchableOpacity activeOpacity={0.5}
                        onPress={() => this.props.toggleButton(this.props.keys, 0)}
                        style={{ ...styles.leftButton, backgroundColor: this.props.leftButtonColor }}>
                        <Text style={{ ...styles.buttonText, color: this.props.leftButtonTextColor }}>{this.props.leftVal}</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.orButtonView}>
                    <TouchableOpacity activeOpacity={0.5}
                        onPress={() => this.props.changeOption(this.props.keys)}
                        style={styles.orButton}>
                        <Text style={styles.buttonText}>OR</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.rightButtonView}>
                    <TouchableOpacity activeOpacity={0.5}
                        onPress={() => this.props.toggleButton(this.props.keys, 1)}
                        style={{ ...styles.rightButton, backgroundColor: this.props.rightButtonColor }}>
                        <Text style={{ ...styles.buttonText, color: this.props.rightButtonTextColor }}>{this.props.rightVal}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 5,
        paddingTop: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    leftButton: {
        zIndex: 2,
        height: 50,
        width: 150,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#000000',
        borderRadius: 8,
        borderWidth: 1,
        alignItems: 'flex-start',
        padding: 5,
    },
    rightButton: {
        zIndex: 2,
        height: 50,
        width: 150,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#000000',
        borderWidth: 1,
        borderRadius: 8,
        alignItems: 'flex-end',
        padding: 5,
    },
    orButton: {
        zIndex: 5,
        padding: 11,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#000000',
        borderWidth: 1,
        borderRadius: 100,
    },

    buttonText: {
        color: '#000000',
        fontSize: fonts.medium,
    },
});

//make this component available to the app
export default Option;
