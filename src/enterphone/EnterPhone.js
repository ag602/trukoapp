//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { fonts } from '../../constants/FontSize';
import GoBack from '../GoBack';

// create a component
class EnterPhone extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countryCode: null,
            mobileNumber: null,
            user:null,
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.goBackView}>
                    <GoBack
                        goToScreen='Sign In'
                        navigation={this.props.navigation}
                    />
                </View>
                <View style={styles.enterPhoneView}>
                    <Text style={styles.enterPhoneText}>Enter Mobile Number:</Text>
                </View>
                <View style={styles.phoneInputView}>
                    <View style={styles.countryCodeView}>
                        <TextInput
                            style={styles.countryCodeInput}
                            onChangeText={(countryCode) =>
                                this.setState({ countryCode })}
                            value={this.state.countryCode}
                            placeholder='+91'
                            placeholderTextColor='black'
                            underlineColorAndroid='transparent'
                        >
                        </TextInput>
                    </View>
                    <View style={styles.phoneView}>
                        <TextInput
                            style={styles.phoneInput}
                            onChangeText={(mobileNumber) =>
                                this.setState({ mobileNumber })}
                            value={this.state.mobileNumber}
                            placeholder='Enter Phone Number'
                            placeholderTextColor='black'
                            keyboardType='numeric'
                            underlineColorAndroid='transparent'
                        >
                        </TextInput>
                    </View>
                </View>
                <View style={styles.getOtpButton}>
                    <TouchableOpacity activeOpacity={0.5}
                        onPress={() => this.props.navigation.navigate('Verify Phone')}
                        style={styles.otpButton}>
                        <Text style={styles.otpButtonText}>Get OTP</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: 'white',
    },
    goBackView: {
        flex: 1,
    },
    enterPhoneView: {
        flex: 4,
        justifyContent: 'center',
        alignItems: 'center',
    },
    phoneInputView: {
        flex: 3,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-around',
    },
    getOtpButton: {
        flex: 5,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    otpButton: {
        zIndex: 5,
        height: 50,
        width: 200,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#000000',
        borderWidth: 1,
    },
    otpButtonText: {
        color: '#000000',
        fontSize: fonts.medium,
        fontWeight: 'bold',
    },
    enterPhoneText: {
        fontSize: fonts.medium,
    },
    countryCodeInput: {
        fontSize: fonts.normal,
        width: 30,
        alignItems: 'center',
        backgroundColor: 'grey',
    },
    phoneInput: {
        fontSize: fonts.normal,
        // borderBottomColor: 'black',
        // borderBottomWidth: 2,
        backgroundColor: 'grey',
        width: 130,
    },
    countryCodeView: {
        flex: 2,
        alignItems: 'center',
    },
    phoneView: {
        flex: 4,
    }
});

//make this component available to the app
export default EnterPhone;
