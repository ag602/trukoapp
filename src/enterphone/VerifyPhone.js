//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import { fonts } from '../../constants/FontSize';
import GoBack from '../GoBack';

// create a component
class VerifyPhone extends Component {
    constructor(props) {
        super(props);
        this.state = {
            digit1: null,
            digit2: null,
            digit3: null,
            digit4: null,
            OTP: null,
        }
        this.checkOTP = this.checkOTP.bind(this);
    }
    componentDidMount = () => {
      // Test when credits refresh
      // const { OTP } =this.props.route.params;
        const { OTP } = 4567;
        this.setState({
            OTP: OTP
        });
        console.log(OTP);
    }
    checkOTP() {
        console.log('OTP is', this.state.OTP);
        let num = this.state.digit1.toString() + this.state.digit2.toString() + this.state.digit3.toString() + this.state.digit4.toString();
        if (num === this.state.OTP) {
            console.log('sucess');
            this.props.navigation.navigate('User Details');
        } else {
            console.log('Not success, You have entered', num);
            alert('OTP is not correct.');
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.goBackView}>
                    <GoBack
                        goToScreen='Sign In'
                        navigation={this.props.navigation}
                    />
                </View>
                <View style={styles.verifyView}>
                    <Text style={styles.verifyText}>
                        Verify Mobile Number:
                    </Text>
                </View>
                <View style={styles.otpRowView}>
                    <View style={styles.otpView}>
                        <Text style={styles.otpTextView}>OTP:</Text>
                    </View>
                    <View style={styles.otpBoxView}>
                        <TextInput
                            style={styles.otpInput}
                            onChangeText={(digit1) =>
                                this.setState({ digit1 })}
                            value={this.state.digit1}
                            // placeholder='+91'
                            placeholderTextColor='gray'
                            keyboardType='numeric'
                            underlineColorAndroid='transparent'
                        >
                        </TextInput>
                        <TextInput
                            style={styles.otpInput}
                            onChangeText={(digit2) =>
                                this.setState({ digit2 })}
                            value={this.state.digit2}
                            // placeholder='+91'
                            placeholderTextColor='gray'
                            keyboardType='numeric'
                            underlineColorAndroid='transparent'
                        >
                        </TextInput>
                        <TextInput
                            style={styles.otpInput}
                            onChangeText={(digit3) =>
                                this.setState({ digit3 })}
                            value={this.state.digit3}
                            // placeholder='+91'
                            placeholderTextColor='gray'
                            keyboardType='numeric'
                            underlineColorAndroid='transparent'
                        >
                        </TextInput>
                        <TextInput
                            style={styles.otpInput}
                            onChangeText={(digit4) =>
                                this.setState({ digit4 })}
                            value={this.state.digit4}
                            // placeholder='+91'
                            placeholderTextColor='gray'
                            keyboardType='numeric'
                            underlineColorAndroid='transparent'
                        >
                        </TextInput>
                    </View>
                </View>
                <View style={styles.nextView}>
                    <TouchableOpacity activeOpacity={0.5}
                        // onPress={() => this.props.navigation.navigate('User Details')}
                        onPress={() => this.checkOTP()}
                        style={styles.button}>
                        <Text style={styles.nextButtonText}>Next</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.resendOtpRowView}>
                    <View style={styles.resendOtpView}>
                        <TouchableOpacity activeOpacity={0.5}
                            // onPress={this.addEntry.bind(this)}
                            style={styles.button}>
                            <Text style={styles.buttonText}>Resend OTP</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.needHelpView}>
                        <TouchableOpacity activeOpacity={0.5}
                            // onPress={this.addEntry.bind(this)}
                            style={styles.button}>
                            <Text style={styles.buttonText}>Need Support?</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: 'white',
    },
    goBackView: {
        flex: 1,
    },
    verifyView: {
        flex: 3,
        alignItems: 'center',
        justifyContent: 'center',
    },
    otpRowView: {
        flex: 2,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    nextView: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    resendOtpRowView: {
        flex: 4,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-around',
    },
    button: {
        zIndex: 5,
        height: 50,
        width: 150,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#000000',
        borderWidth: 1,
    },
    nextButtonText: {
        color: '#000000',
        fontSize: fonts.medium,
        fontWeight: 'bold',
    },
    buttonText: {
        color: '#000000',
        fontSize: fonts.medium,
    },
    verifyText: {
        fontSize: fonts.medium,
    },
    otpView: {
        flex: 4,
        alignItems: 'center',
    },
    otpBoxView: {
        flex: 6,
        alignItems: 'center',
        flexDirection: 'row',
    },
    otpInput: {
        fontSize: fonts.normal,
        width: 30,
        margin: 5,
        backgroundColor: 'grey',
    },
    otpTextView: {
        fontSize: fonts.medium,
        fontWeight: 'bold',
    }
});

//make this component available to the app
export default VerifyPhone;
