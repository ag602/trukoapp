//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import { RadioButton } from 'react-native-paper';
import UploadPic from './UploadPic'
import DatePicker from '../userdetails/DatePicker';
import { fonts } from '../../constants/FontSize';
import GoBack from '../GoBack';

// create a component
class UserDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            checked: 'female',
            date: "26-01-2000",
        }
    }
    render() {
        const { checked } = this.state;
        return (
            <View style={styles.container}>
                <View style={styles.goBackView}>
                    <GoBack
                        goToScreen='Verify Phone'
                        navigation={this.props.navigation}
                    />
                </View>
                <View style={styles.detailsView}>
                    <View style={styles.personalDetailsView}>
                        <Text style={styles.personalDetailsText}>
                            Personal Details: </Text>
                    </View>
                    <View style={styles.listView}>
                        <View style={styles.leftView}>
                            <Text style={styles.leftText}>Name: </Text>
                        </View>
                        <View style={styles.rightView}>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={(name) =>
                                    this.setState({ name })}
                                value={this.state.name}
                                // placeholder='Enter your name'
                                placeholderTextColor='gray'
                                underlineColorAndroid='transparent'
                            >
                            </TextInput>
                        </View>
                    </View>
                    <View style={styles.listView}>
                        <View style={styles.leftView}>
                            <Text style={styles.leftText}>Gender: </Text>
                        </View>
                        <View style={styles.rightView}>
                            <RadioButton
                                value="female"
                                status={checked === 'female' ? 'checked' : 'unchecked'}
                                onPress={() => { this.setState({ checked: 'female' }); }}
                                color="grey"
                            />
                            <Text style={styles.genderText}>Female</Text>
                            <RadioButton
                                value="male"
                                status={checked === 'male' ? 'checked' : 'unchecked'}
                                onPress={() => { this.setState({ checked: 'male' }); }}
                                color="grey"
                            />
                            <Text style={styles.genderText}>Male</Text>
                            <RadioButton
                                value="others"
                                status={checked === 'others' ? 'checked' : 'unchecked'}
                                onPress={() => { this.setState({ checked: 'others' }); }}
                                color="grey" color="grey"
                            />
                            <Text style={styles.genderText}>Others</Text>
                        </View>
                    </View>
                    <View style={styles.listView}>
                        <View style={styles.leftView}>
                            <Text style={styles.leftText}>Date of Birth: </Text>
                        </View>
                        <View style={styles.datePickerView}>
                            <DatePicker />
                        </View>
                    </View>
                    <View style={styles.listView}>
                        <View style={styles.leftView}>
                            <Text style={styles.leftText}>Upload Display Pic: </Text>
                        </View>
                        <View style={styles.rightView}>
                            <UploadPic />
                        </View>
                    </View>
                    <View style={styles.listView}>
                        <View style={styles.leftView}>
                        </View>
                        <View style={styles.imageWarningView}>
                            {/*<Text style={styles.imageWarningText}>
                                JPG, max. 50 Kb. Face should be clear
                                </Text>*/}
                        </View>
                    </View>
                </View>
                <View style={styles.nextButtonView}>
                    <TouchableOpacity activeOpacity={0.5}
                        onPress={() => this.props.navigation.navigate('Options')}
                        style={styles.nextButton}>
                        <Text style={styles.nextButtonText}>Next</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: 'white',
    },
    goBackView: {
        flex: 1,
    },
    detailsView: {
        flex: 7,
    },
    nextButtonView: {
        flex: 3,
        alignItems: 'center',
    },
    personalDetailsView: {
        flex: 3,
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingBottom: 10,
    },
    listView: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    leftView: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        marginLeft: 20,
    },
    rightView: {
        marginRight: 20,
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
    },
    textInput: {
        borderBottomColor: 'black',
        borderBottomWidth: 2,
        width: 200,
    },
    leftText: {
        fontSize: fonts.normal,
        fontWeight: 'bold',
    },
    personalDetailsText: {
        fontSize: fonts.medium,
    },
    nextButton: {
        zIndex: 5,
        height: 50,
        width: 200,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#000000',
        borderWidth: 1,
    },
    nextButtonText: {
        color: '#000000',
        fontSize: fonts.medium,
        fontWeight: 'bold',
    },
    imageWarningView: {
        marginRight: 20,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    imageWarningText: {
        fontSize: fonts.small,
    },
    datePickerView: {
        alignItems: 'stretch',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    genderText: {
        fontSize: fonts.normal
    }
});

//make this component available to the app
export default UserDetails;
