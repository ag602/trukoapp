//For more - https://enappd.com/blog/pick-images-from-camera-gallery-in-react-native-app/78/
import React, { Fragment, Component } from 'react';
import ImagePicker from 'react-native-image-picker';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Button,
  Dimensions,
  TouchableOpacity
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
export default class ImagePickerExample extends Component {
  constructor(props) {
    super(props)
    this.state = {
      filepath: {
        data: '',
        uri: ''
      },
      fileData: '',
      fileUri: ''
    }
  }

  chooseImage = () => {
    let options = {
      title: 'Select Image',
      customButtons: [
        { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response.fileName);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        // alert(JSON.stringify(response));s
        // console.log('response', JSON.stringify(response));
        this.setState({
          filePath: response,
          fileData: response.data,
          fileUri: response.uri,
          fileName:response.fileName,
        });
      }
    });
  }

  launchCamera = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = { uri: response.uri };
        console.log('response', JSON.stringify(response));
        this.setState({
          filePath: response,
          fileData: response.data,
          fileUri: response.uri,
          fileName:response.fileName,
        });
      }
    });

  }

  launchImageLibrary = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = { uri: response.uri };
        console.log('response', JSON.stringify(response));
        this.setState({
          filePath: response,
          fileData: response.data,
          fileUri: response.uri,
          fileName:response.fileName,
        });
      }
    });

  }

  // renderFileData() {
  //   if (this.state.fileData) {
  //     return <Image source={{ uri: 'data:image/jpeg;base64,' + this.state.fileData }}
  //       style={styles.images}
  //     />
  //   } else {
  //     return <Image source={require('./google.png')}
  //       style={styles.images}
  //     />
  //   }
  // }
  renderFileName() {
  console.log(this.state.fileName);
    if (this.state.fileName) {
      return <Text>{this.state.fileName}</Text>
    } else {
      return <Text></Text>
    }
  }

  // renderFileUri() {
  //   if (this.state.fileUri) {
  //     return <Image
  //       source={{ uri: this.state.fileUri }}
  //       style={styles.images}
  //     />
  //   } else {
  //     return <Image
  //       source={require('./google.png')}
  //       style={styles.images}
  //     />
  //   }
  // }
  render() {
    return (
          <View style={styles.body}>
          {/*   <Text style={{textAlign:'center',fontSize:20,paddingBottom:10}} >Pick Images from Camera & Gallery</Text>*/}


            <View style={styles.btnParentSection}>
              <TouchableOpacity onPress={this.chooseImage} style={styles.btnSection}  >
                <Text style={styles.btnText}>Choose File</Text>
              </TouchableOpacity>


              {/*  <TouchableOpacity onPress={this.launchCamera} style={styles.btnSection}  >
                <Text style={styles.btnText}>Directly Launch Camera</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={this.launchImageLibrary} style={styles.btnSection}  >
                <Text style={styles.btnText}>Directly Launch Image Library</Text>
              </TouchableOpacity>*/}
            </View>
            <View style={styles.ImageSections}>
            {/* <View>
                {this.renderFileData()}
                <Text  style={{textAlign:'center'}}>Base 64 String</Text>
              </View>*/}
              <View>
                {this.renderFileName()}
                <Text style={{textAlign:'center'}}></Text>
              </View>
            </View>

          </View>

    );
  }
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  ImageSections: {
    display: 'flex',
    flexDirection: 'row',
    paddingHorizontal: 8,
    paddingVertical: 8,
    justifyContent: 'center',
    marginTop:-10
  },
  images: {
    width: 100,
    height: 100,
    borderColor: 'black',
    borderWidth: 1,
    marginHorizontal: 3,
  },
  btnParentSection: {
    alignItems: 'center',
    marginTop:10
  },
  btnSection: {
    width: 225,
    height: 50,
    backgroundColor: '#DCDCDC',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
    marginTop:40
  },
  btnText: {
    textAlign: 'center',
    color: 'gray',
    fontSize: 14,
    fontWeight:'bold'
  }
});

// import * as React from 'react';
// import { Button, Image, View, Text, StyleSheet, TouchableOpacity } from 'react-native';
// import ImagePicker from 'react-native-image-picker';
// import { fonts } from '../../constants/FontSize';
//
// export default class ImagePickerExample extends React.Component {
//     state = {
//         image: null,
//     };
//
//     render() {
//         let { image } = this.state;
//
//         return (
//             <View style={styles.container}>
//                 <TouchableOpacity activeOpacity={0.5}
//                     onPress={this._pickImage}
//                     style={styles.uploadImageButton}>
//                     <Text style={styles.uploadImageText}>Choose File</Text>
//                 </TouchableOpacity>
//                 {image && <Text style={styles.imageMsg}>Image Seleted</Text>}
//                 {!image && <Text style={styles.imageMsg}>No file chosen</Text>}
//                 {/* {image &&
//           <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />} */}
//             </View>
//         );
//     }
//
//     componentDidMount() {
//         this.getPermissionAsync();
//     }
//
//     getPermissionAsync = async () => {
//         if (Constants.platform.ios) {
//             const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
//             if (status !== 'granted') {
//                 alert('Sorry, we need camera roll permissions to make this work!');
//             }
//         }
//     }
//
//     _pickImage = async () => {
//         let result = await ImagePicker.launchImageLibraryAsync({
//             mediaTypes: ImagePicker.MediaTypeOptions.All,
//             allowsEditing: true,
//             aspect: [4, 3],
//             quality: 1
//         });
//
//         console.log(result);
//
//         if (!result.cancelled) {
//             this.setState({ image: result.uri });
//         }
//     };
// }
//
// const styles = StyleSheet.create({
//     container: {
//         flexDirection: 'row',
//         alignItems: 'center',
//     },
//     uploadImageButton: {
//         height: 30,
//         width: 90,
//         backgroundColor: '#591e44',
//         alignItems: 'center',
//         justifyContent: 'center',
//     },
//     uploadImageText: {
//         color: 'white',
//         fontSize: fonts.normal,
//         alignItems: 'center',
//         justifyContent: 'center',
//     },
//     imageMsg: {
//         padding: 10,
//         fontSize: fonts.normal,
//     }
// })
