import React, { useState } from 'react';
import { Image, View, Button, Platform, Text, TouchableOpacity, StyleSheet } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import logo from '../../assets/images/calendar.png';
import { fonts } from '../../constants/FontSize';
import UserDetails from './UserDetails';

const DatePicker = () => {
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    date.setMinutes(date.getMinutes() + 330);
    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
    };

    const showMode = currentMode => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };
    let dateVar = date;
    dateVar = JSON.stringify(dateVar);
    dateVar = dateVar.substring(1, 11)
    var p = dateVar.split(/\D/g)
    dateVar = [p[2], p[1], p[0]].join("-")

    // console.log('datae', JSON.stringify(date));
    return (
        <View style={styles.container}>
            <View style={styles.calenderView}>
                <TouchableOpacity activeOpacity={0.5}
                    onPress={showDatepicker}>
                    <Image source={logo} style={{ width: 40, height: 40 }} />
                </TouchableOpacity>
            </View>
            <View style={styles.dateView}>
                <Text style={styles.dateText}>{dateVar}</Text>
            </View>

            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    timeZoneOffsetInMinutes={0}
                    value={date}
                    maximumDate={new Date(2021, 10, 20)}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                />
            )}
        </View>
    );
};

export default DatePicker;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 20,
    },
    calenderView: {
            marginRight: 30,
    },
    dateView: {
        backgroundColor: 'grey',
        padding: 10,
        borderColor: 'black',
        borderWidth: 1,
    },
    dateText: {
        color: 'white',
        fontSize: fonts.normal,
    }
})
