//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { WebView } from 'react-native-webview';

// create a component
class WebViews extends Component {
    render() {
        return (
            <WebView source={{ uri: 'https://google.com' }} style={{ marginTop: 20 }} />
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'skyblue',
    },
});

//make this component available to the app
export default WebViews;
