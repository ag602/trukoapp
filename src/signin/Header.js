//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import  { fonts }  from '../../constants/FontSize';


// create a component
class Header extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.logo}>truko</Text>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    logo: {
        color: '#591e44',
        fontSize: fonts.xlarge,
        fontWeight: 'bold',
    }
});

//make this component available to the app
export default Header;
