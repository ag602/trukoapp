//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { fonts } from '../../constants/FontSize';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import VerifyPhone  from './src/enterphone/VerifyPhone';
// create a component


class Otp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mobileNumber: '',
    }

  }

    sendOtp = async () => {
        console.log('Get OTP Pressed')
        // let user = 'shriya chandrakar';
        // let password = '@Qwerty123';
        let msisdn = this.state.mobileNumber;
        // let sid = 'SIHIND';
        let name = 'Customer';
        let OTP = '';
        while (OTP.length !== 4) {
            OTP = Math.floor(Math.random() * 10000);
            OTP = OTP.toString();
        }
        console.log('OTP', OTP);
        console.log('Mobile Number', msisdn);
        let phoneReg = /^\d{10}$/;
        if (msisdn.match(phoneReg)) {
            console.log('Correct Phone Number');
            try {
                   const response = await fetch(`http://cloud.smsindiahub.in/vendorsms/pushsms.aspx?user=shriya chandrakar&password=@Qwerty123&msisdn=${msisdn}&sid=SIHIND&msg=Dear ${name}, your OTP is ${OTP}.&fl=0&gwid=2`);
                   const data =  await response.json();
                   console.log(data);
                   if (data.ErrorMessage === "Success") {
                    this.props.navigation.navigate('VerifyPhone', { OTP: OTP});
                  } else if (data.ErrorMessage == "Insufficient credits") {
                    alert("Insufficient Credits. OTP won't be send. Only test redirect!")
                    //Do this when credits refresh again.
                    // this.props.navigation.navigate('VerifyPhone', { OTP: OTP});
                    this.props.navigation.navigate('Verify Phone');
                  } else {
                    alert('Please enter valid phone number.');
                   }
            } catch ({ message }) {
                console.log('error', message);
            }
        } else {
            alert('Please enter valid phone number.');
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topSignIn}>
                    <View style={styles.signInView}>
                        <Text style={styles.singInText}>Sign In</Text>
                    </View>
                    <View style={styles.mobileNumberView}>
                        <View style={styles.mobileNumberText}>
                            <Text style={styles.mobileText}>Mobile No :  </Text>
                        </View>
                        <View style={styles.mobileNumberInput}>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={(mobileNumber) =>
                                    this.setState({ mobileNumber })}
                                value={this.state.mobileNumber}
                                placeholder='Enter Phone No.'
                                placeholderTextColor='gray'
                                keyboardType='numeric'
                                underlineColorAndroid='transparent'
                            >
                            </TextInput>
                        </View>
                    </View>
                </View>

                <View style={styles.getOtp}>
                    <TouchableOpacity activeOpacity={0.5}
                        onPress={() => this.sendOtp()}
                        style={styles.otpButton}>
                        <Text style={styles.otpButtonText}>Get OTP</Text>
                    </TouchableOpacity>
                </View>
              </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: 'white',
    },
    topSignIn: {
        flex: 3,
        alignItems: 'stretch',
        backgroundColor: 'white',
    },
    getOtp: {
        flex: 3,
        backgroundColor: 'white',
    },
    signInView: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    mobileNumberView: {
        flex: 2,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    mobileNumberText: {
        flex: 4,
        alignItems: 'flex-end',
    },
    mobileText: {
        fontSize: fonts.normal,
    },
    mobileNumberInput: {
        flex: 6,
        alignItems: 'flex-start',
    },
    textInput: {
        // alignSelf: 'center',
        fontSize: fonts.normal,
    },
    singInText: {
        fontSize: fonts.medium,
    },
    getOtp: {
        flex: 3,
        backgroundColor: 'white',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    otpButton: {
        zIndex: 5,
        height: 50,
        width: 200,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#000000',
        borderWidth: 1,
    },
    otpButtonText: {
        color: '#000000',
        fontSize: fonts.medium,
        fontWeight: 'bold',
    }

});

//make this component available to the app
export default Otp;
