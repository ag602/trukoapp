import React from "react";
import { Text, TouchableOpacity, StyleSheet, Image } from "react-native";

const styles = StyleSheet.create({
  linkedinStyle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#4875B4",
    borderWidth: 0.5,
    borderColor: "#fff",
    height: 60,
    width: 60,
    borderRadius: 100,
  },
  imageIconStyle: {
    padding: 13,
    height: 30,
    width: 30,
    resizeMode: "stretch"
  }
});

export class LinkedInSocialButton extends React.Component {
  render() {
    return (
      <TouchableOpacity
        style={{ ...styles.linkedinStyle }}
        onPress={this.props.onPress}
      >
        <Image
          source={require("../../assets/images/linkedin.png")}
          style={{...styles.imageIconStyle}}
        />
      </TouchableOpacity>
    );
  }
}
