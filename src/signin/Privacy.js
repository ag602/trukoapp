//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Linking } from 'react-native';
import { fonts } from '../../constants/FontSize';

// create a component
class Privacy extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.privacyText}>
                    By signing up, you agree to our &nbsp;
                    <Text style={styles.linkingText}
                        onPress={() => Linking.openURL('https://google.com')}>
                        Terms & Condition
                    </Text>
                    &nbsp; & &nbsp;
                    <Text style={styles.linkingText}
                        onPress={() => Linking.openURL('https://google.com')}>
                        Privacy Policy
                    </Text>
                </Text>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    privacyText: {
        fontSize: fonts.small
    },
    linkingText: {
        color: 'steelblue',
    }
});

//make this component available to the app
export default Privacy;
