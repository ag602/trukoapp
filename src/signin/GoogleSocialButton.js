import React from "react";
import { Text, TouchableOpacity, StyleSheet, Image } from "react-native";

const styles = StyleSheet.create({
  googleStyle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#ffffff",
    borderWidth: 0.5,
    borderColor: "#fff",
    height: 60,
    width: 60,
    borderRadius: 100,
  },
  imageIconStyle: {
    padding: 10,
    height: 30,
    width: 30,
    resizeMode: "stretch"
  },
});

export class GoogleSocialButton extends React.Component {
  render() {
    return (
      <TouchableOpacity
        style={{ ...styles.googleStyle}}
        onPress={this.props.onPress}
      >
        <Image
          source={require("../../assets/images/google.png")}
          style={{ ...styles.imageIconStyle}}
        />
      </TouchableOpacity>
    );
  }
}
