import React from "react";
import { Text, TouchableOpacity, StyleSheet, Image, View} from "react-native";

const styles = StyleSheet.create({
  facebookStyle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#485a96",
    borderWidth: 0.5,
    borderColor: "#fff",
    height: 60,
    width: 60,
    borderRadius: 100,
  },
  imageIconStyle: {
    padding: 10,
    height: 30,
    width: 30,
    resizeMode: "stretch"
  },
});

export class FacebookSocialButton extends React.Component {
  render() {
    return (
      <TouchableOpacity
        style={{ ...styles.facebookStyle }}
        onPress={this.props.onPress}
      >
        <Image
          source={require("../../assets/images/facebook.png")}
          style={{...styles.imageIconStyle}}
        />
      </TouchableOpacity>
    );
  }
}
