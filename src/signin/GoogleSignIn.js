//import liraries
import React, { Component, Fragment } from 'react';
import { View, Text, StyleSheet, Button, TouchableOpacity } from 'react-native';
import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import { GoogleSocialButton } from "./GoogleSocialButton";
// import auth from '@react-native-firebase/auth';
//import * as firebase from 'firebase';
import { withNavigation } from '@react-navigation/native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


//Guide to googlesignin - https://dev.to/anwargul0x/get-started-with-react-native-google-sign-in-18i5
// https://enappd.com/blog/google-login-in-react-native-android-apps-with-firebase/90/

// create a component
class GoogleSignIn extends Component {
    constructor(props) {
        super(props);
        this.onLoginPress = this.onLoginPress.bind(this);// you should bind this to the method that call the props
        this.state = {
        pushData: [],
        loggedIn: false
    };
}
//    componentDidMount() {
//      GoogleSignin.configure({
//       // scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
//       scopes: ['profile', 'email'],
//       webClientId: '810871628306-fi606ro8hkdr0ujpttfeo2o1bdjcpb8u.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
//       offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
//       hostedDomain: '', // specifies a hosted domain restriction
//       loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
//       forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
//       accountName: '', // [Android] specifies an account name on the device that should be used
//       iosClientId: '', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
//     });
// }

componentDidMount(){
  GoogleSignin.configure({
   // scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
   scopes: ['profile', 'email'],
   webClientId: '810871628306-fi606ro8hkdr0ujpttfeo2o1bdjcpb8u.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
   offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
   // hostedDomain: '', // specifies a hosted domain restriction
   // loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
   forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
   // accountName: '', // [Android] specifies an account name on the device that should be used
   // iosClientId: '', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
 });
};


signIn = async () => {
  try {

    await GoogleSignin.hasPlayServices();
    const userInfo = await GoogleSignin.signIn();
    this.setState({ userInfo:userInfo, loggedIn:true });
    console.log(userInfo);
    const isSignedIn = await GoogleSignin.isSignedIn();
    console.log(isSignedIn);
    // if (isSignedIn){
    //   console.log('bye');
    //   this.props.navigation.navigate('UserDetails');
    // }
    // else{
    //   console.log('hi');
    //   this.props.navigation.navigate('UserDetails');
    // }
    // console.log(GoogleSignin.hasPlayServices());
    // console.log(this.state.userInfo.user.email);
  } catch (error) {
    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      // user cancelled the login flow
    } else if (error.code === statusCodes.IN_PROGRESS) {
      // operation (e.g. sign in) is in progress already
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      // play services not available or outdated
    } else {
      console.log(error,'Some error occurred. Please try later');
      // some other error happened
    }
  }
};
onLoginPress = async () => {
  var result = await this.signIn()
  console.log(this.state.userInfo);
  let user_info =  this.state.userInfo;
  try {
    this.props.navigation.navigate('Enter Phone',{user:user_info});
  } catch (e) {
    console.log(e);
  }
  // if there is no result.error or result.cancelled, the user is logged in
  // do something with the result
}
// //When user logs in once, on next app start you don’t want user to login again.
// //Hence you try to silently login the user.
// //If the user does not have a valid session, this(getCurrentUserInfo) method will return null and you can ask user to sign in
// getCurrentUserInfo = async () => {
//   try {
//     const userInfo = await GoogleSignin.signInSilently();
//     console.log(userInfo)
//     this.setState({ userInfo:userInfo, loggedIn: true });
//
//   } catch (error) {
//     if (error.code === statusCodes.SIGN_IN_REQUIRED) {
//       // user has not signed in yet
//       this.setState({ loggedIn: false });
//     } else {
//       // some other error
//       this.setState({ loggedIn: false });
//     }
//   }
// };
//
isSignedIn = async () => {
const isSignedIn = await GoogleSignin.isSignedIn();
console.log(isSignedIn);
this.setState({ isLoginScreenPresented: !isSignedIn });
};
//
// signOut = async () => {
//   try {
//     await GoogleSignin.revokeAccess();
//     await GoogleSignin.signOut();
//     this.setState({ user: null, loggedIn: false }); // Remember to remove the user from your app's state as well
//   } catch (error) {
//     console.error(error);
//   }
// };

    render() {
        return (
            <View style={styles.container}>
                 <GoogleSocialButton
                    onPress={() => this.onLoginPress()}
                />
            </View>


        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
});

//make this component available to the app
export default GoogleSignIn;
