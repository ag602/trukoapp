//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { LinkedInSocialButton } from './LinkedInSocialButton';

// create a component
class LinkedInSignIn extends Component {
    render() {
        return (
            <View style={styles.container}>
                <LinkedInSocialButton
                    onPress={() => alert('button linkedin')}
                />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
});

//make this component available to the app
export default LinkedInSignIn;
