//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { FacebookSocialButton } from "./FacebookSocialButton";
// import { LoginButton } from 'react-native-fbsdk';
import { LoginManager } from 'react-native-fbsdk';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import UserDetails from '../userdetails/UserDetails';

// create a component
class FacebookSignIn extends Component {
    constructor(props) {
        super(props);
        this.logIn = this.logIn.bind(this);// you should bind this to the method that call the props
        //Setting the state for the data after login
        this.state = {
        user_name: '',
        token: '',
        profile_pic: '',
        public_profile:'',
        user_friends:'',
      };
    }

    // Attempt a login using the Facebook login dialog,
    // asking for default permissions.
    logIn () {
        LoginManager.logInWithPermissions(['public_profile','email', 'user_friends']).then(
          this.function = (result) => {
            if (result.isCancelled) {
              console.log('Login cancelled')
            } else {
              console.log(result);
              // const { navigate } = this.props.navigation;
              // console.log(this.state.public_profile);
              this.props.navigation.navigate('User Details',{user:this.state.public_profile})
              console.log('Login success with permissions: ' + result.grantedPermissions.toString())
            }
          },
          function (error) {
            console.log('Login fail with error: ' + error)
          }
        )
      }

    render() {
        return (
            <View style={styles.container}>
                <FacebookSocialButton
                    onPress={() => this.logIn()}
                />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
});

//make this component available to the app
export default FacebookSignIn;
