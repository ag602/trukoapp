//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Linking } from 'react-native';
import { fonts } from '../../constants/FontSize';
import GoogleSignIn from './GoogleSignIn';
import FacebookSignIn from './FacebookSiginIn';
import LinkedInSignIn from './LinkedInSignIn';

// create a component
class SocialMedia extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.orView}>
                    <Text style={styles.orViewText}>OR</Text>
                </View>
                <View style={styles.signUpUsingView}>
                    <Text style={styles.signUpUsingText}>Sign Up using : </Text>
                </View>
                <View style={styles.socialMediaButtonsView}>
                    <View style={styles.buttonsView}>
                        <FacebookSignIn navigation={this.props.navigation} />
                    </View>
                    <View style={styles.buttonsView}>
                        <GoogleSignIn navigation={this.props.navigation} />
                    </View>
                    <View style={styles.buttonsView}>
                        <LinkedInSignIn />
                    </View>
                </View>
                <View style={styles.emailView}>
                    <Text style={styles.emailText}>Or use email ID to sign up by clicking &nbsp;
                        <Text style={styles.linkingText}
                            onPress={() => Linking.openURL('https://google.com')}>
                            here
                        </Text>
                    </Text>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: 'white',
    },
    orView: {
        flex: 2,
        alignItems: 'center',
    },
    signUpUsingView: {
        flex: 2,
        alignItems: 'center',
    },
    socialMediaButtonsView: {
        flex: 3,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    emailView: {
        flex: 2,
        alignItems: 'center',
    },
    orViewText: {
        fontSize: fonts.large,
        fontWeight: 'bold',
    },
    signUpUsingText: {
        fontSize: fonts.medium,
    },
    socialMediaButtons: {
        zIndex: 5,
        borderRadius: 100,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#000000',
        borderWidth: 2,
    },
    linkingText: {
        color: 'steelblue',
        fontSize: fonts.normal
    },
    emailText: {
        fontSize: fonts.normal,
    },
    buttonsView: {
        alignItems: 'center',
        justifyContent: 'center',
    }
});

//make this component available to the app
export default SocialMedia;
