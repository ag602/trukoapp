//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import Header from './Header';
import Otp from './Otp';
import SocialMedia from './SocialMedia';
import Privacy from './Privacy';

var { height, width } = Dimensions.get('window');

// create a component
class SignIn extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <View style={styles.container}>

                <View style={styles.header}>
                    <Header />
                </View>
                <View style={styles.otp}>
                    <Otp navigation={this.props.navigation}/>
                </View>
                <View style={styles.socialMedia}>
                    <SocialMedia navigation={this.props.navigation}/>
                </View>
                <View style={styles.privacy}>
                    <Privacy />
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
    },
    header: {
        flex: 3,
    },
    otp: {
        flex: 5,
    },
    socialMedia: {
        flex: 5,
    },
    privacy: {
        flex: 1,
    }
});

//make this component available to the app
export default SignIn;
