//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import previous from '../assets/images/previous.png';

// create a component
class GoBack extends Component {
    constructor(props) {
        super(props);
    }
    render() {
      const {navigate} = this.props.navigation
        return (
            <View style={styles.container}>
                <TouchableOpacity activeOpacity={0.5}
                    onPress={() => navigate(this.props.goToScreen)}
                    style={styles.goBackButton}
                >
                    <Image source={previous} style={{ width: 40, height: 40 }} />
                </TouchableOpacity>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        backgroundColor: 'white',
        margin: 15,
    },
    goBackButton: {
        zIndex: 10,
        // height: 200,
        // width: 200,
    }
});

//make this component available to the app
export default GoBack;
