const xlarge = 40;
const large = 30;
const medium = 20;
const normal = 14;
const small = 10;

export const fonts =  {
    xlarge,
    large,
    medium,
    normal,
    small,
  }
